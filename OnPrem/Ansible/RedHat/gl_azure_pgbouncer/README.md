Role Name
=========

This role conigured PGBouncer Specifically for Azure. Azure does not support MD5 Auth so overwriting the
database.ini and pg_auth along with a customer gitlab.rb. This will need to be done anytime a reconfigure
happens and is probably not supported by support. It's reccomended that this only be deployed in an 
Environment that stricly follows immutable pipeline driven IaC.


Requirements
------------

gitlabhq_production is the assumed database name. Port 5432 is also assumed on the database. SSL is also assumed
in this iteration.

Role Variables
--------------

postgres_user                   : Postgres User
pg_password                     : Postgres Password
postgres_master                 : Postgres Address
pgbouncer_cert_name             : Name of Cert files used in the gl_cert_importer role

Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com