roles ['redis_master_role']

## Enable Redis
redis['enable'] = true

## Disable all other services
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
unicorn['enable'] = false
postgresql['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
alertmanager['enable'] = false
pgbouncer_exporter['enable'] = false
gitaly['enable'] = false

# Must be the same in every sentinel node
redis['master_name'] = 'gitlab-redis'
redis['bind'] = '{{ ansible_eth0.ipv4.address }}'
redis['port'] = 6379
redis['password'] = '{{ redis_password }}'

# roles ['redis_slave_role']
# redis['master_ip'] = '<%= $::gitlab::master_redis_node_ip %>'

gitlab_rails['auto_migrate'] = false

# Monitoring
redis_exporter['enable'] = true
#redis_exporter['flags'] = { 'redis.addr' => 'redis://<%= $::gitlab::host_reachable_ip %>' }
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'
redis_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9121'

# Enable service discovery for Prometheus
consul['enable'] = true
consul['monitoring_service_discovery'] = true

consul['configuration'] = {
  bind_addr: '{{ ansible_eth0.ipv4.address }}',
  retry_join: %w({{ consul_servers }})
}