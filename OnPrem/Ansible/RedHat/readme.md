# Inventory.yaml from Terrraform Sandbox

1. Verify the terraform executable. I use terraform11 and terraform12 binaries so make sure it's valid for version 11.
2. Make sure the Terraform in ../../puppet/terraform-aws-sandbox/amazonlinux2/ has recently been run and the outputs are valid.
3. Validate ../../puppet/terraform-aws-sandbox/amazonlinux2/ regions / keys and outputs are specific to your testing.
3. Run build_inventory.sh to build inventory.yaml

## SSH Local configuration

In order to access trough the Bastion without to pass the key every single time, it will be required to add the bastion entry (you can get the DNS runnint `terraform output bastion`) in the file .ssh/config as follows:

```
Host bastion
  Hostname <BASTION_PUBLIC_DNS>
  ForwardAgent yes
  IdentityFile ~/Documents/GitLab/aws_keys/afonseca-paris.pem
  ControlPath ~/.ssh/cm-%r@%h:%p
  ControlMaster auto
  ControlPersist 10m

#puppet server
Host ip-*
  IdentityFile ~/Documents/GitLab/aws_keys/afonseca-paris.pem
  ProxyCommand ssh ec2-user@bastion -W %h:%p
  ControlPersist
```

Obs: here we are assuming amazon linux as distro in Debia you would replace `ec2-user` for `admin`

Try to ssh to the bastion adding the ssh public key to your known_hosts file (answering yes when asked it); 

When ssh(ing) for any node if you get any error like:

```
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:3yAzcViUZxBdYiwZ184tDKyJN5TTb/yd5Jvyt1px1/c.
Please contact your system administrator.
Add correct host key in /Users/adriano/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /Users/adriano/.ssh/known_hosts:162
ECDSA host key for ip-10-9-0-9.eu-west-3.compute.internal has changed and you have requested strict checking.
Host key verification failed.
```

This means that you have a old public key your ~/.ssh/known_hosts and you will need to clean it.

# Seed SSH Keys

We seed the ssh keys as per recommendation.

https://docs.gitlab.com/ee/administration/high_availability/gitlab.html#extra-configuration-for-additional-gitlab-application-servers

# Running Playbooks

In order to run the playbooks you can either use ansible.cfg to set the default inventory.yml or pass it as parameter ans shown in the sample code below. We have a playbook that import all the others `gl_deploy_gitlab`. However, it is better to deploy nodes, one role at time and to verify that gitlab is running after to go to the next. The orther below must be preserved to fullfil dependency between components.

## Consul
1. ansible-playbook -i inventory.yaml consul_sentinel.yml

## Redis
1. ansible-playbook -i inventory.yaml redis_master.yml
2. ansible-playbook -i inventory.yaml redis.yml

## PostgreSQL
1. ansible-playbook -i inventory.yaml pg_db_master.yml
2. ansible-playbook -i inventory.yaml pg_db.yml

## Rails
1. ansible-playbook -i inventory.yaml gl_rails.yml

## Gitaly
1. ansible-playbook -i inventory.yaml gitaly.yml

# Cleaning a Server
> Warning!, this will remove gitlab and all data on the nodes.
1. Update hosts: "{{ variable_host | default('gitlab_app') }}" to point to the default application you would like to clean
2. ansible-playbook -i inventory.yaml clean.yml