Role Name
=========

This role can be used to initialize a Cloud based Volume for Gitaly

Requirements
------------



Role Variables
--------------

mount_point                         : Example /dev/sdc 
init_db                             : Boolean Variable used to decide if the gitaly drive initalization should be run



Dependencies
------------

Azure Storage Account with File Shares Enabled
Ability to install azure-cli and cifs-utils form YUM

Author Information
------------------

kvogt@gitlab.com