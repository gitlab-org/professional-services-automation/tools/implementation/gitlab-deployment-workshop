# Disable all components except Pgbouncer and Consul agent
roles ['pgbouncer_role']

pgbouncer_exporter['enable'] = true

# Configure Pgbouncer
pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)

# Configure Consul agent
consul['watchers'] = %w(postgresql)

# START user configuration
# Please set the real values as explained in Required Information section
# Replace CONSUL_PASSWORD_HASH with with a generated md5 value
# Replace PGBOUNCER_PASSWORD_HASH with with a generated md5 value
pgbouncer['users'] = {
  'gitlab-consul': {
    password: '{{gitlab_consul_users_pw_hash}}'
  },
  'pgbouncer': {
    password: '{{pgbouncer_users_pw_hash}}'
  }
}

#######################################
###     Monitoring configuration    ###
#######################################
consul['enable'] = true

consul['configuration'] = {
  bind_addr: '{{ ansible_eth0.ipv4.address }}',
  retry_join: %w({{ consul_servers }})
}

# Enable service discovery for Prometheus
consul['monitoring_service_discovery'] =  true

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'

# Disable all components except Pgbouncer and Consul agent
roles ['pgbouncer_role']
gitlab_rails['auto_migrate'] = false
pgbouncer['enable'] = true
# Configure Pgbouncer
pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)


# START user configuration
# Please set the real values as explained in Required Information section
# Replace CONSUL_PASSWORD_HASH with with a generated md5 value
# Replace PGBOUNCER_PASSWORD_HASH with with a generated md5 value
#pgbouncer['users'] = {
#  'gitlab-consul': {
#    password: '{{gitlab_consul_users_pw_hash}}'
#  },
#  'pgbouncer': {
#    password: '{{pgbouncer_users_pw_hash}}'
#  }
#}
## PGBouncer Databases
#pgbouncer['databases'] = {
#  {% for db_server in pgbouncer_database_servers %}
#  gitlabhq_production: {
#    host: '{{db_server}}',
#    user: 'pgbouncer',
#    password: '{{pgbouncer_users_pw_hash}}'
#  },
#  {% endfor %}
#}

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'
pgbouncer_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9188'