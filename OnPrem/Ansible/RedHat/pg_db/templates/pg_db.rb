{% if geo_enabled %}
roles ['geo_primary_role', 'postgres_role']
{% else %}
roles ['postgres_role']
{% endif %}


postgresql['port'] = 5432
postgresql['listen_address'] = '{{ ansible_eth0.ipv4.address }}'
postgresql['hot_standby'] = 'on'
postgresql['wal_level'] = 'replica'
postgresql['shared_preload_libraries'] = 'repmgr_funcs'


gitlab_rails['auto_migrate'] = false


# This password is: `echo -n '${password}${username}' | md5sum - | cut -d' ' -f1`
# The default username is `gitlab`
postgresql['sql_user_password'] = '{{ password_hash }}'
postgresql['pgbouncer_user_password'] = '{{ pgbouncer_users_pw_hash }}'

# This needs to be set to 1 higher than the number of clusters to 
# prevent replication taking up to many connections
postgresql['max_wal_senders'] = {{ wal_count }}
postgresql['max_replication_slots'] = {{ wal_count }}

# Configure the CIDRs for MD5 authentication
# Configure the CIDRs for trusted authentication (passwordless)

{% if geo_enabled %}
postgresql['md5_auth_cidr_addresses'] = {{ postgresql_md5_auth_cidr_addresses_geo }}
{% else %}
postgresql['md5_auth_cidr_addresses'] = '{{ postgresql_trust_auth_cidr_addresses }}'
{% endif %}

postgresql['trust_auth_cidr_addresses'] = {{ postgresql_trust_auth_cidr_addresses }}
repmgr['trust_auth_cidr_addresses'] = {{ postgresql_trust_auth_cidr_addresses }}



# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = "{{ ansible_eth0.ipv4.address }}:9100"
postgres_exporter['listen_address'] = "{{ ansible_eth0.ipv4.address }}:9187"

# workaround for the error
# level=info msg="Error opening connection to database (user=gitlab-psql%20host=0.0.0.0%20database=postgres%20sslmode=allow): pq: unsupported sslmode \"allow\"; only \"require\" (default), \"verify-full\", \"verify-ca\", and \"disable\" supported" source="postgres_exporter.go:1012"
postgres_exporter['env']['DATA_SOURCE_NAME'] = "user=gitlab password='{{ password_hash }}' host=127.0.0.1 database=postgres sslmode=disable"

# Set to approximately 1/4 of available RAM.
#postgresql['shared_buffers'] = {{ postgres_shared_buffer }}

#postgres_exporter['env']['DATA_SOURCE_NAME'] = "user=gitlab_repmgr host=127.0.0.1 database=gitlab_repmgr sslmode=disable"



## Disable everything else
# sidekiq['enable'] = false
# unicorn['enable'] = false
# registry['enable'] = false
# gitaly['enable'] = false
# gitlab_workhorse['enable'] = false
# nginx['enable'] = false
# prometheus_monitoring['enable'] = false
# redis['enable'] = false


# Enable service discovery for Prometheus
consul['services'] = %w(postgresql)
consul['monitoring_service_discovery'] = true
consul['configuration'] = {
  bind_addr: '{{ ansible_eth0.ipv4.address }}',
  retry_join: %w({{ consul_servers }})
}
repmgr['master_on_initialization'] = false