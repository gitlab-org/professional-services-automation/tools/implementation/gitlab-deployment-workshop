Role Name
=========

This role Configures Gitaly

Requirements
------------

gitlab-secrets.json in the templates directory. Should be fetched after
rails run.

Role Variables
--------------

external_url                : The URL Used for GitLab
gitaly_token                : The Secret Share Key for Gitaly. 


Optional

gitaly_tls                  : true or false. Is Gitaly going to use certificates?
gitaly_cert_name            : If using Certificates include the gl_cert_importer. The Role will install 
                              the certificate as {{gitaly_cert_name}}.crt and {{gitaly_cert_name}}.key
praefect_int_token          : The PreShared Interal Token if Praefect is being used.

Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com