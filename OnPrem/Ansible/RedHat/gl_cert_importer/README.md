Role Name
=========

This role can be used to Add SSL Certificates to any Gitlab Node

Requirements
------------

Place all of the /etc/gitlab/ssl certificate files in the files/ssl directory.
Place all of the /etc/gitlab/trusted-certs certificate files in the files/trusted-certs directory.

Role Variables
--------------

Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com