praefect['enable'] = true
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
grafana['enable'] = false
puma['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
gitaly['enable'] = false
node_exporter['listen_address'] = '0.0.0.0:9100'

# Prevent database connections during 'gitlab-ctl reconfigure'
gitlab_rails['rake_cache_clear'] = false
gitlab_rails['auto_migrate'] = false

# praefect['env'] = {
#   'GITALY_DISABLE_REF_TRANSACTIONS' => 'true'
# }


{% if not praefect_tls %}
praefect['listen_addr'] = '0.0.0.0:2305'
{% endif %}

{% if praefect_tls %}
praefect['tls_listen_addr'] = "0.0.0.0:{{praefect_port}}"
praefect['certificate_path'] = '/etc/gitlab/ssl/{{praefect_cert_name}}.crt'
praefect['key_path'] = '/etc/gitlab/ssl/{{praefect_cert_name}}.key'
{% endif %}

# Enable Prometheus metrics access to Praefect. You must use firewalls
# to restrict access to this address/port.
praefect['prometheus_listen_addr'] = '0.0.0.0:9652'

praefect['auth_token'] = '{{praefect_ext_token}}'

praefect['database_host'] = '{{ praefect_db_host }}'
praefect['database_port'] = 5432
praefect['database_user'] = '{{postgres_user}}'
praefect['database_password'] = '{{ praefect_sql_password }}' 
praefect['database_dbname'] = 'praefect_production'

{% if praefect_tls %}
praefect['database_sslmode'] = '{{praefect_database_sslmode}}'
{% endif %}

# Name of storage hash must match storage name in git_data_dirs on GitLab
# server ('praefect') and in git_data_dirs on Gitaly nodes ('gitaly-1')
praefect['virtual_storages'] = {
  'default' => {
    'gitaly-1' => {
      'address' => '{{gitaly_protocol}}://{{ gitaly_host_1 }}:{{gitaly_port}}',
      'token'   => '{{praefect_int_token}}',
      'primary' => true
    },
    'gitaly-2' => {
      'address' => '{{gitaly_protocol}}://{{ gitaly_host_2 }}:{{gitaly_port}}',
      'token'   => '{{praefect_int_token}}'
    },
    'gitaly-3' => {
      'address' => '{{gitaly_protocol}}://{{ gitaly_host_3 }}:{{gitaly_port}}',
      'token'   => '{{praefect_int_token}}'
    }
  }
}
