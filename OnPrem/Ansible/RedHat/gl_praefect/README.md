Role Name
=========

This role Configures Praefect

Requirements
------------

Place gitlab-secrets.json in the templates directory. praefect_production database name is assumed.
SSL On the database is required 

Role Variables
--------------

external_url                : The URL Used for GitLab
gitaly_token                : The Secret Share Key for Gitaly. 
gitaly_protocol             : tcp or tls
gitaly_host_1               : Gitaly Host in cluster
gitaly_host_2               : Gitaly Host in cluster
gitaly_host_3               : Gitaly Host in cluster
gitaly_port                 : Most Likely 8075 or 9999 for TLS

Optional
praefect_database_sslmode     : set to require for cloud SSL
praefect_tls                  : true or false. Is Praefect going to use certificates?
praefect_cert_name            : If using Certificates include the gl_cert_importer. The Role will install 
                                the certificate as {{praefect_cert_name}}.crt and {{praefect_cert_name}}.key
praefect_int_token            : The PreShared Interal Token
praefect_ext_token            : The PreShared External Token
praefect_db_host              : Praefect DataBase HostName
postgres_user                 : Praefect SQL User


Dependencies
------------

None 

Author Information
------------------

kvogt@gitlab.com