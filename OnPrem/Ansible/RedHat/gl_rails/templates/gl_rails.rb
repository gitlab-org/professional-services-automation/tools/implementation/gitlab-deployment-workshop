external_url '{{ external_url }}'

nginx['redirect_http_to_https'] = {{redirect_http_to_https|lower}}
letsencrypt['enable'] = false

# registry_nginx['redirect_http_to_https'] = true

{% if pages_enabled is defined %}
pages_external_url 'https://{{external_url}}'
gitlab_pages['enable'] = true
# gitlab_pages['inplace_chroot'] = true
pages_nginx['redirect_http_to_https'] = true
pages_nginx['ssl_certificate'] = "/etc/gitlab/ssl/{{rails_cert_name}}.crt"
pages_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/{{rails_cert_name}}.key"
{% endif %}

gitlab_rails['auto_migrate'] = false
gitlab_rails['db_adapter'] = 'postgresql'
gitlab_rails['db_encoding'] = 'unicode'
postgresql['enable'] = false
{% if gravatar_enabled is defined %}
gitlab_rails['gravatar_enabled'] = {{gravatar_enabled}}
{% endif %}

gitlab_rails['db_database'] = 'gitlabhq_production'
gitlab_rails['db_host'] = '{{pgbouncer_host}}'
gitlab_rails['db_port'] = {{db_port}}

{% if pgbouncer_users_pg_bouncer_pw is defined %}
gitlab_rails['db_username'] = "gitlab"
gitlab_rails['db_password'] = '{{pgbouncer_users_pg_bouncer_pw}}'
{% endif %}
{% if pgbouncer_users_pg_bouncer_pw is not defined %}
gitlab_rails['db_username'] = '{{postgres_user}}'
gitlab_rails['db_password'] = '{{pg_password}}'
{% endif %}

{% if geo_enabled is defined and geo_enabled %}
roles ['geo_primary_role']
gitlab_rails['geo_node_name'] = '{{geo_node_name}}'
{% endif %}

{% if smtp_enabled is defined and smtp_enabled %}
gitlab_rails['smtp_enable'] = true  
gitlab_rails['smtp_address'] = "{{smtp_address}}"                                                                                              
gitlab_rails['gitlab_email_from'] = '{{smtp_reply}}'
gitlab_rails['gitlab_email_reply_to'] = '{{gitlab_email_reply_to}}'
gitlab_rails['smtp_user_name'] = '{{smtp_user}}' 
gitlab_rails['smtp_password'] = '{{smtp_pass}}' 
gitlab_rails['smtp_authentication'] = "login"
gitlab_rails['smtp_enable_starttls_auto'] = true
{% endif %}

unicorn['enable'] = false
puma['enable'] = true
puma['listen'] = '{{ ansible_eth0.ipv4.address }}'

{% if ldap_enabled is defined and ldap_enabled %}
gitlab_rails['ldap_sync_worker_cron'] = "0 */2 * * *"
gitlab_rails['ldap_enabled'] = true
gitlab_rails['prevent_ldap_sign_in'] = true
gitlab_rails['ldap_servers'] = {
'main' => {
  'label' => 'LDAP',
  'host' =>  '{{ldap_host}}',
  'port' => {{ldap_port}},
  'uid' => 'sAMAccountName',
  'encryption' => 'plain',
  'verify_certificates' => true,
  'bind_dn' => '{{ldap_bind_dn}}',
  'password' => '{{ldap_password}}',
  'tls_options' => {
    'ca_file' => '',
    'ssl_version' => '',
    'ciphers' => '',
    'cert' => '',
    'key' => ''
  },
  'timeout' => 30,
  'active_directory' => true,
  'allow_username_or_email_login' => false,
  'block_auto_created_users' => false,
  'base' => '{{ldap_base}}',
  'user_filter' => '(&(objectClass=user)(objectCategory=person)(sAMAccountName=*))',
  'attributes' => {
    'username' => ['sAMAccountName'],
    'email' => ['mail'],
    'name' => 'cn',
    'first_name' => 'givenName',
    'last_name' => 'sn'
  },
  'lowercase_usernames' => false,

  'group_base' => '{{ldap_group_base}}',
  'admin_group' => '',
  'external_groups' => [],
  'sync_ssh_keys' => false
  }
}
{% endif %}

# # Disable User Name Changing
# gitlab_rails['gitlab_username_changing_enabled'] = false

# Disable password reset by users
#gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'azure_oauth2'

# gitlab_rails['omniauth_allow_single_sign_on'] = ['azure_oauth2']
# gitlab_rails['omniauth_external_providers'] = ['azure_oauth2']
# gitlab_rails['omniauth_auto_link_ldap_user'] = true
# gitlab_rails['omniauth_block_auto_created_users'] = false
# gitlab_rails['gitlab_default_can_create_group'] = false


# gitlab_rails['omniauth_providers'] = [
#   {
#     "name" => "azure_oauth2",
#     "args" => {
#       "client_id" => "auth_azure_client_id",
#       "client_secret" => "auth_azure_client_secret",
#       "tenant_id" => "auth_azure_tenant_id",
#     }
#   },
#   {
#     "name" => "github",
#     "app_id" => "ghub_appid",
#     "app_secret" => "ghub_secret",
#     "url" => "ghub_url",
#     "args" => { "scope" => "user:email" }
#   } 
# ]

### Disables Redis ###
redis['enable'] = false
gitlab_rails['redis_host'] = '{{redis_master}}'
gitlab_rails['redis_port'] = '{{redis_port}}'
{% if redis_password is defined %}
gitlab_rails['redis_password'] = '{{redis_password}}'
{% endif %}
{% if redis_ssl is defined %}
#Enable for Azure SSL
gitlab_rails['redis_ssl'] = {{redis_ssl|lower}}
{% endif %}

## A list of sentinels with `host` and `port`
# Redis Sentinel nodes
{% if redis_sentinel_list is defined %}
## The same password for Redis authentication you set up for the master node.
redis['master_password'] = '{{redis_password}}'
gitlab_rails['redis_sentinels'] = [
{% for node in redis_sentinel_list %}
   {'host' => "{{node}}", 'port' => 26379 },
{% endfor %}
]
{% endif %}


gitaly['enable'] = false

{% if praefect_ext_token is not defined %}
git_data_dirs({
   'default' => { 'gitaly_address' => 'tcp://{{ gitaly_host }}:8075' },
})
gitlab_rails['gitaly_token'] = '{{gitaly_token}}'
{% endif %}

{% if praefect_ext_token is defined %}
git_data_dirs({
   "default" => {
     "gitaly_address" => "{{praefect_protocol}}://{{ praefect_host }}:{{praefect_port}}",
     "gitaly_token" => "{{praefect_ext_token}}"
   }
 })
gitlab_shell['secret_token'] = '{{gitaly_token}}'
{% endif %}

prometheus['enable'] = false
grafana['enable'] = false

gitlab_rails['object_store']['connection'] = {
  'provider' => '{{object_store_provider}}',
  'region' => '{{object_storage_region}}',
{% if object_storage_access_key_id is defined %}
  'azure_storage_account_name' => '{{object_storage_access_key_id}}',
  'azure_storage_access_key' => '{{object_storage_secret_access_key}}',
{% endif %}
{% if object_storage_access_key_id is not defined %}
  'use_iam_profile' => true
{% endif %}
}

gitlab_rails['lfs_object_store_direct_upload'] = true
gitlab_rails['uploads_object_store_direct_upload'] = true
# gitlab_rails['uploads_object_store_background_upload'] = false

gitlab_rails['object_store']['enabled'] = true
gitlab_rails['object_store']['objects']['artifacts']['bucket'] = '{{artifacts_bucket}}'
gitlab_rails['object_store']['objects']['external_diffs']['bucket'] = '{{externaldiffs_bucket}}'
gitlab_rails['object_store']['objects']['lfs']['bucket'] = '{{lfs_bucket}}'
gitlab_rails['object_store']['objects']['uploads']['bucket'] = '{{uploads_bucket}}'
gitlab_rails['object_store']['objects']['packages']['bucket'] = '{{packages_bucket}}'
gitlab_rails['object_store']['objects']['dependency_proxy']['bucket'] = '{{dependencyproxy_bucket}}'
gitlab_rails['object_store']['objects']['terraform_state']['bucket'] = '{{terraformstate_bucket}}'
{% if pages_bucket is defined and pages_bucket %}
#Required 13.7 minimum for Pages Bucket
gitlab_rails['object_store']['objects']['pages']['bucket'] = '{{pages_bucket}}'
{% endif %}

#################################
#  Registry Configuration
#################################
# registry['enable'] = false
# #registry_external_url '{{external_url}}:4567'
# gitlab_rails['registry_api_url'] = '{{external_url}}:4567'
# gitlab_rails['registry_enabled'] = false

# Required with HAProxy SSL termination
#registry_nginx['proxy_set_headers'] = {
#"X-Forwarded-Proto" => "https",
#"X-Forwarded-Ssl" => "on"
#}

# Docs at https://docs.gitlab.com/ee/administration/geo/replication/docker_registry.html
# registry['notifications'] = [
#   {
#     'name' => 'geo_event',
#     'url' => '{{external_url}}/api/v4/container_registry_event/events',
#     'timeout' => '500ms',
#     'threshold' => 5,
#     'backoff' => '1s',
#     'headers' => {
#       'Authorization' => ['redacted']
#     }
#   }
# ]

# Azure Storage
# registry['storage'] = {
#   'azure' => {
#     accountname: 'object_storage_access_key_id',
#     accountkey: 'object_storage_secret_access_key',
#     container: 'registry'
#   },
  # 's3' => {
  # 'region' => 'object_storage_region',
  # 'accesskey' => 'object_storage_access_key_id',
  # 'secretkey' => 'object_storage_secret_access_key',
  # 'bucket' => 'gl-registry',
  # 'regionendpoint' => 'object_storage_endpoint_url',
  # 'endpoint' => 'object_storage_endpoint_url',
  # 'path_style' => true,
  # 'aws_signature_version' => 2
  # }
# }
# registry['debug_addr'] = "localhost:5001"
# registry['log_level'] = "debug"




#################################
# Backups
#################################
# 7 Days, this will only prune local backups.
# Not ones that have been copied to S3
gitlab_rails['backup_keep_time'] = 604800
gitlab_rails['backup_upload_remote_directory'] = '{{ backup_bucket }}'
gitlab_rails['backup_upload_connection'] = {
'provider' => '{{object_store_provider}}',
'region' => '{{object_storage_region}}',

{% if object_storage_access_key_id is defined %}
'aws_access_key_id' => '{{object_storage_access_key_id}}',
'aws_secret_access_key' => '{{object_storage_secret_access_key}}'
{% endif %}
{% if object_storage_access_key_id is not defined %}
'use_iam_profile' => true
{% endif %}

# The below options configure an S3 compatible host instead of AWS
{% if object_storage_endpoint_url is defined %}
'endpoint' => '{{object_storage_endpoint_url}}',
'path_style' => true,
{% if object_storage_signature is defined %}
'aws_signature_version' => {{object_storage_signature}}
{% endif %}
{% endif %}
}


#######################################
###     Monitoring configuration    ###
######################################

#https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/3764
alertmanager['flags'] = {
  'cluster.advertise-address' => "{{ ansible_eth0.ipv4.address }}:9093",
}


# Enable service discovery for Prometheus
consul['enable'] = false
consul['monitoring_service_discovery'] =  false

{% if consul_servers is defined %}
# Consul server nodes
consul['configuration'] = {
   bind_addr: '{{ ansible_eth0.ipv4.address }}',
   advertise_addr: '{{ ansible_eth0.ipv4.address }}',
   retry_join: %w({{consul_servers}}),
}
{% endif %}

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '0.0.0.0:9100'
gitlab_exporter['listen_address'] = '0.0.0.0'
gitlab_exporter['listen_port'] = '9168'

gitlab_workhorse['prometheus_listen_addr'] = '0.0.0.0:9229'
sidekiq['listen_address'] = '{{ ansible_eth0.ipv4.address }}'
sidekiq['metrics_enabled'] = true

gitlab_rails['monitoring_whitelist'] = ['{{monitoring_whitelist}}']

{% if monitoring_ip is defined %}
nginx['status']['options'] = {
      "server_tokens" => "off",
      "access_log" => "off",
      "allow" => "{{monitoring_ip}}",
      "deny" => "all",
}
{% endif %}

#######################################
###  END Monitoring configuration   ###
#######################################

{% if geo_secondary is defined and geo_secondary %}
gitlab_rails['geo_node_name'] = '{{geo_node_name}}'
roles ['geo_secondary_role']

{% if tracking_db_user is defined %}
geo_secondary['db_username'] = '{{tracking_db_user}}'
geo_secondary['db_password'] = '{{tracking_db_password}}'
geo_secondary['db_host'] = '{{geo_tracking_db_host}}'
geo_secondary['db_port'] =  5432    # change to the correct port
geo_postgresql['enable'] = false     # don't use internal managed instance
geo_secondary['db_database'] = 'gitlabhq_geo_production'
{% endif %}
{% endif %}
