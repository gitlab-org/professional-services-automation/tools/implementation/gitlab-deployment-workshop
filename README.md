# GitLab Deployment Workshop
:wave: Welcome to the GitLab deployment workshop. This is part of the [certified implementation engineer specialist](https://about.gitlab.com/services/pse-certifications/implementation-specialist/) learning journey and is required to earn your certification. 

In this exercise we will deploy and configure HA Gitlab on AWS using Ansible and Terraform. 

To start, create an issue from the [deployment-workshop issue template here](https://gitlab.com/gitlab-org/professional-services-automation/tools/implementation/gitlab-deployment-workshop/-/issues/new?issuable_template=deployment-workshop)
