variable gitaly_count {
  default = 3
}

variable praefect_count {
  default = 3
}

variable praefect_ips {
}

variable ami_id {
  type = string
}

variable "praefect_port" {
  description = "Port for Praefect Health Check"
}

variable vpc_id {
  type = string
}

variable private_subnets {
  type = list
}

variable key_name {
  type = string
}

variable vpc_cidr {
  type = string
}

variable gitaly_instance_type {
  type = string
}

variable praefect_instance_type {
  type = string
}


variable git_size {
  type    = string
  default = 300
}

variable praefect_dbadmin {
  type = string
}


variable "praefect_multiaz" {
  type = bool
  default = false
}