# Creating a classic load balancer

# resource "aws_elb_attachment" "praefect" {
#   count    = var.praefect_count
#   elb      = aws_elb.elb.id
#   instance = aws_instance.praefect_server[count.index].id
# }


# resource "aws_elb" "elb" {
#   name = "praefect-elb"
#   #   availability_zones = var.availability_zones
#   subnets = var.private_subnets
#   internal           = true

#   listener {
#     instance_port     = 2305
#     instance_protocol = "tcp"
#     lb_port           = 2305
#     lb_protocol       = "tcp"
#   }

#   listener {
#     instance_port     = 3305
#     instance_protocol = "tcp"
#     lb_port           = 3305
#     lb_protocol       = "tcp"
#   }

#   health_check {
#     healthy_threshold   = 2
#     unhealthy_threshold = 2
#     timeout             = 3
#     target              = "TCP:${var.praefect_port}"
#     interval            = 30
#   }
#   security_groups           = [aws_security_group.alb_praefect.id]
#   cross_zone_load_balancing = true
# }

#network loadbalance
resource "aws_lb" "nlb" {
  name               = "praefect-nlb"
  internal           = true
  load_balancer_type = "network"
  subnets            = var.private_subnets
  enable_cross_zone_load_balancing = true
}

resource "aws_lb_listener" "listener" {
    load_balancer_arn       = aws_lb.nlb.arn
    port              = "2305"
    protocol          = "TCP"
    default_action {
     type             = "forward"
     target_group_arn = aws_lb_target_group.nlb.arn
  }
}

resource "aws_lb_target_group" "nlb" {
  name     = "gitlab-nlb-listeners"
  port     = var.praefect_port
  protocol = "TCP"
  vpc_id   = var.vpc_id

  health_check {
    port     = var.praefect_port
    protocol = "TCP"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_target_group_attachment" "nlb" {
  count            = var.praefect_count
  target_group_arn = aws_lb_target_group.nlb.arn
  target_id        = aws_instance.praefect_server[count.index].id
  port             = 2305
}

# target groups
# resource "aws_lb_target_group" "alb" {
#   name     = "gitlab-listeners"
#   port     = var.praefect_port
#   protocol = "TCP"
#   vpc_id   = var.vpc_id

#   health_check {
#     port     = var.praefect_port
#     protocol = "TCP"
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }

# lb security group
# resource "aws_security_group" "alb_praefect" {
#   name   = "praefect-elb"
#   vpc_id = var.vpc_id

#   ingress {
#     from_port   = 2305
#     to_port     = 2305
#     protocol    = "tcp"
#     cidr_blocks = ["${var.vpc_cidr}"]
#   }

#   ingress {
#     from_port   = 3305
#     to_port     = 3305
#     protocol    = "tcp"
#     cidr_blocks = ["${var.vpc_cidr}"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
# }