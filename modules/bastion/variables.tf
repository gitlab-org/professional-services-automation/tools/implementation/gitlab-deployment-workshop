variable ami_id {
    description = "AMI ID for Instance Launch"
}

variable key_name {
    description = "AWS Key Pair for SSH"
}

variable "instance_type" {
    description = "Instance Type"
    default = "t3.small"
}

variable "subnet_id" {
    description = "Public Subnet for Bastion"
}

variable "vpc_id" {
    description = "VPC For the Instance"
}