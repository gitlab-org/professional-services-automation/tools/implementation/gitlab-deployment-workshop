resource "aws_security_group" "bastion" {
  name        = "gitlab-bastion-sg"
  description = "Bastion Security Group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "bastion" {
  vpc = true
  instance                  = aws_instance.bastion.id
}

resource "aws_instance" "bastion" {
  instance_type = var.instance_type
  ami           = var.ami_id
  key_name      = var.key_name
  subnet_id     = var.subnet_id
  vpc_security_group_ids = [aws_security_group.bastion.id]
  tags = {
    Name = "Gitlab-Bastion"
  }
  user_data = base64encode(templatefile("${path.module}/templates/user_data.yml",{}))

}
