concurrent = 10
check_interval = 0
log_level = "debug"

[session_server]
  session_timeout = 1800

[[runners]]
  name = "${RunnerName}"
  url = "${GitLabURL}"
  token = "GitLabRegToken"
  executor = "docker+machine"
  limit = 20
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.machine]
    IdleCount = 1
    IdleTime = 1800
    MaxBuilds = 10
    MachineDriver = "amazonec2"
    MachineName = "gitlab-docker-machine-%s"
    MachineOptions = [
      "amazonec2-iam-instance-profile=${iam_role_name}",
      "amazonec2-region=${region}",
      "amazonec2-vpc-id=${vpc_id}",
      "amazonec2-subnet-id=${subnet_id}",
      "amazonec2-use-private-address=true",
      "amazonec2-zone=${zone}",
      "amazonec2-private-address-only=true",
      "amazonec2-tags=runner-manager-name,gitlab-aws-autoscaler,gitlab,true,gitlab-runner-autoscale,true",
      "amazonec2-security-group=${security-group}",
      "amazonec2-instance-type=m4.2xlarge",
    ]
