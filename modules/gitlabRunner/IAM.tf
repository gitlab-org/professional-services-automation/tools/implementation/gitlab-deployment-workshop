resource "aws_iam_instance_profile" "gitlab-runner" {
  name = "gitlab-runner-nodes"
  path = aws_iam_role.gitlab-runner.path
  role = aws_iam_role.gitlab-runner.name
}

resource "aws_iam_instance_profile" "gitlab-runner2" {
  name = "gitlab-runner-nodes2"
  path = aws_iam_role.gitlab-runner.path
  role = aws_iam_role.gitlab-runner2.name
}

resource "aws_iam_role" "gitlab-runner" {
  name               = "gitlab-runner-nodes"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.gitlab-runner-assume-role.json
}

resource "aws_iam_role" "gitlab-runner2" {
  name               = "gitlab-runner-nodes2"
  path               = "/"
  assume_role_policy = data.aws_iam_policy_document.gitlab-runner-assume-role.json
}

resource "aws_iam_role_policy" "gitlab-runner" {
  policy = data.aws_iam_policy_document.gitlab-runner.json
  role   = aws_iam_role.gitlab-runner.name
  name   = "gitlab-runner-nodes"
}
# This is just for GitLab Internal testing so a key pair is not needed
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.gitlab-runner.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}
data "aws_iam_policy_document" "gitlab-runner-assume-role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      identifiers = ["ec2.amazonaws.com"]
      type        = "Service"
    }
  }
}
data "aws_iam_policy_document" "gitlab-runner" {
  statement {
    effect    = "Allow"
    actions   = ["s3:*", "ec2:*", "iam:PassRole"]
    resources = ["*"]
  }
}
