resource "aws_security_group" "gitlab-runner" {
  name        = "gitlab-runner-sg"
  description = "gitlab runner security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    security_groups = [var.gla-security-group]
  }
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [var.gla-security-group]
    cidr_blocks     = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 2376
    to_port         = 2376
    protocol        = "tcp"
    security_groups = [var.gla-security-group]
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [var.gla-security-group]
  }

  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

locals {
  template_runner_config = templatefile("${path.module}/templates/runner-config.tpl", {
    GitLabURL         = var.gitlab-url
    GitLabRegToken    = var.registration-token
    RunnerName        = "Runner Name"
    RunnerDescription = "A Runner"
    vpc_id            = var.vpc_id
    subnet_id         = var.vpc_private_subnet_ids[0]
    iam_role_name     = aws_iam_role.gitlab-runner2.name
    region            = "us-west-2"
    zone              = "a"
    security-group    = aws_security_group.gitlab-runner.name
  })
}

resource "aws_launch_configuration" "gitlab-runner" {
  name_prefix   = "gitlab-launch-configuration-"
  image_id      = var.gitlab-runner-ami
  instance_type = var.gitlab-runner-instance-type

  iam_instance_profile        = aws_iam_instance_profile.gitlab-runner.arn
  associate_public_ip_address = false
  enable_monitoring           = true

  security_groups = [aws_security_group.gitlab-runner.id]

  root_block_device {
    volume_size           = "200"
    delete_on_termination = true
  }
  #  key_name = aws_key_pair.my_key.key_name

  user_data = templatefile("${path.module}/templates/user_data.tpl", {
    runner_config     = local.template_runner_config
    GitLabURL         = var.gitlab-url
    GitLabRegToken    = var.registration-token
    RunnerName        = "Runner Name"
    RunnerDescription = "A Runner"
  })
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_placement_group" "gitlab-runner" {
  name     = "gitlab-runner"
  strategy = "cluster"
}

resource "aws_autoscaling_group" "gitlab-runner" {
  name                 = "gitlab-runner-nodes"
  max_size             = 1
  min_size             = 1
  desired_capacity     = 1
  default_cooldown     = 300
  health_check_type    = "EC2"
  launch_configuration = aws_launch_configuration.gitlab-runner.name

  vpc_zone_identifier = [var.vpc_private_subnet_ids[0]]

  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "gitlab-runner"
  }

  lifecycle {
    ignore_changes = [desired_capacity]
  }
}

