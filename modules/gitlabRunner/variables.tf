variable "gitlab-runner-ami" {
  type = string
}

variable "gitlab-url" {
  type = string
}

variable "registration-token" {
  type = string
}

variable "gitlab-runner-instance-type" {
  type = string
}

variable "gla-security-group" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "vpc_private_subnet_ids" {
  type = list
}

variable "tags" {
  type        = map(string)
  description = "map of tags to put on the resource"
  default     = {}
}

