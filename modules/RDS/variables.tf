variable "prefix" {}

variable "database_name" {
  type    = string
  default = "gitlabhq_production"
}

variable "database_admin" {
  type    = string
  default = "gitlab"
}

variable "vpc_id" {
  type = string
}

variable "vpc_cidr" {
  type = string
}

# variable "hosted_zone_id" {
#   type = string
# }

#variable rds_endpoint {
#  type = string
#}

variable "private_subnets" {
  type = list
}

variable "postgres_port" {
  type    = string
  default = "5432"
}

variable "instance_class" {
  type = string
}

variable "allocated_storage" {
  type = string
}

variable "engine_version" {
  type = string
}

variable "storage_type" {
  type = string
}

variable "engine" {
  type = string
}

variable "final_snapshot_identifier" {
  type = string
}

variable "multi_az" {
  type = string
}

variable "auto_minor_version_upgrade" {
  type = string
}

variable "tags" {
  type        = map(string)
  description = "map of tags to put on the resource"
  default     = {}
}
