output "rds_arn" {
  value = aws_db_instance.gitlab-pg.arn
}
output "rds_id" {
  value = aws_db_instance.gitlab-pg.id
}
output "rds_azs" {
  value = aws_db_instance.gitlab-pg.availability_zone
}
output "rds_endpoint" {
  value = aws_db_instance.gitlab-pg.address
}
output "rds_db_name" {
  value = var.database_name
}
output "rds_admin_user" {
  value = aws_db_instance.gitlab-pg.username
}
output "rds_admin_pass" {
  value = aws_secretsmanager_secret_version.gitlab-postgres-password.secret_string
}
